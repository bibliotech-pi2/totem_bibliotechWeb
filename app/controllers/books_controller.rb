class BooksController < ApplicationController
	before_action :set_book, only: [:show]

	def index
		search_book(params[:book_search])
		@book_search = params[:book_search]
	end

	def show
		unless @book.condition_deactivate 
			@schedules = @book.schedules
		else
			redirect_to books_path
		end
	end

	private 
	def set_book
		@book = Book.find(params[:id])
	end

	def book_params
		params.require(:book).permit(:author_one, :author_two, :author_three, :title,
		 :subtitle, :publisher, :year, :city, :edition, :volume, :pages, :ISBN, :barcode)
	end

	def search_book(book_search)
		if !book_search.blank?
			@books = Book.where(condition_deactivate: false).search_full_text("#{book_search}")
		else
			@books = Book.where(condition_deactivate: false)
		end
	end
end
