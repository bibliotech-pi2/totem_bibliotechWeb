class UserNotifierMailer < ApplicationMailer
	 default :from => 'bibliotech@bibliotech.solutions'
    # send a signup email to the user, pass in the user object that   contains the user's email address

    def send_email_to_loaded(user, loan, book)
    	puts "!"*80
        @user = user
        @loan = loan
        @book = book
        mail(
            to:  @user.email,
            subject: 'Bibliotech - Livro Emprestado: '+@book.title)
    end
end