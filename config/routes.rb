Rails.application.routes.draw do
	root 'books#index'

	#books
	get 'books' => 'books#index', :as => 'books'
	post 'books' => 'books#index', :as => 'books_post'

	#loans
	post 'loans/create/:book_id	' => 'loans#create', :as => 'create_loan'
	get 'loans/new/:book_id' => 'loans#new', :as => 'new_loan'
	get 'loans/index' => 'loans#index', :as => 'loan_index'
	post 'loans/pre_return_book' => 'loans#pre_return_book', :as => 'loans_pre_return_book'
	post'loans/return_book' => 'loans#return_book', :as => 'loans_return_book'
	get 'loans' => 'loans#index', :as => 'loans'
	get 'return_robot/:case_id' => 'loans#return_robot', :as =>'return_robot'

	#robot
	get 'wait_robot/:case_id' => 'robot#wait_robot', :as =>'wait_robot'
	

	resources :books, except:[:index, :create, :update, :destroy]
end
