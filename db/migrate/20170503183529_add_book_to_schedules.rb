class AddBookToSchedules < ActiveRecord::Migration[5.0]
  def change
    add_reference :schedules, :book, foreign_key: true
  end
end
