class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :first_schedule_user

  def is_defaulting_user?
    if @user.loans == []
      return false
    else
      @user.loans.each do |loan|
     		if loan.real_return_date == nil
     			if loan.return_date < Time.now
     				return true
          else 
            return false
     			end
        else
          return false
     		end
     	end
    end
  end
 	
  def first_schedule_user?(book, user)
  	first_schedule = book.schedules.first

  	if !first_schedule.nil?
  		if user.id == first_schedule.user_id
	  		return true
	  	else
	  		return false
	  	end
	  	return false
	  end
  end
end