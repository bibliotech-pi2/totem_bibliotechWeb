class LoansController < ApplicationController

	include LoansHelper

	def new
		@loan = Loan.new
		@book = Book.find(params[:book_id])
	end

	def create
		@loan = false
		book = Book.find(params[:book_id])
		@user = User.find_by(matricula: params[:loan_context][:matricula])
		if @user.nil? || !@user.authenticate(params[:loan_context][:password])
			redirect_to new_loan_path book.id
			flash[:warning] = 'Matrícula ou senha inválidos!'
		else 
			if !is_defaulting_user?
				if book.available && (book.schedules.empty? || first_schedule_user?(book, @user))
					@loan = Loan.new
					@loan.book_id = book.id
					@loan.user_id = @user.id
					@loan.loan_date = Time.now
					@loan.return_date = Time.now + 15.days
					book_case = Case.find(book.case.id)
					#request_to_raspberry(:get, book.case.x_position , book.case.y_position)
					if @loan.save
						book.update_attribute(:available, false)
						free_case(book)
						if first_schedule_user?(book, @user)
							if book.schedules.first.user_id == @user.id
								book.schedules.first.destroy
							end
						end
						redirect_to wait_robot_path book_case.id
						flash[:success] = 'Empréstimo solicitado. O período máximo para a devolução do livro é: '+@loan.return_date.to_s
						UserNotifierMailer.send_email_to_loaded(@loan.user, @loan, @loan.book).deliver_now
					else
						redirect_to book
						flash[:warning] = 'Não foi possível solicitar o empréstimo'
						
					end
				else
					if !book.available
						flash[:info] = "Livro já emprestado"
					end
					if !book.schedules.empty? && !first_schedule_user?(book, @user)
						flash[:warning] = "Este livro está agendado para outro usuário"
					end
					
					redirect_to book
				end
			else
				redirect_to '/'
				flash[:warning] = 'Você está inadiplente! Devolva os livros atrasados para realizar novos empréstimos'
			end
		end
	end

	def index
	end

	def pre_return_book
		@user = User.find_by(matricula: params[:user][:matricula])
		if @user.nil? || !@user.authenticate(params[:user][:password])
			redirect_to loan_index_path
			flash[:warning] = 'Matrícula ou senha inválidos!'
		end
	end

	def return_book
		@user = User.find(params[:user_id])

		if @user.nil?
			redirect_to loan_index_path
			flash[:warning] = 'Erro. Usuário não encontrado!'
		else
			book = Book.find_by(barcode: params[:return_book][:barcode])
			if book.nil?
				redirect_to loan_index_path
				flash[:warning] = 'Livro não encontrado!'
			else
				@user.loans.each do |loan|
					if loan.book_id == book.id && loan.real_return_date == nil
						if set_case(book)

							loan.update_attribute(:real_return_date, Time.now)
							book.update_attribute(:available, true)
							set_schedules(book)
							#request_to_raspberry(:get, book.case.x_position, book.case.y_position)
							redirect_to wait_robot_path book.case.id
							flash[:success] = 'Livro devolvido com sucesso'
							return
						else
							flash[:warnig] = 'Operação cancelada! Não existe case disponível para o livro!'
							redirect_to loan_index_path
						end
					end
				end
				flash[:warning] = 'Você não possui um empréstimo registrado desse livro'
				redirect_to loan_index_path
			end
		end		 
	end

	def return_robot
		puts("$" * 80)
		puts("Case id: " + params[:case_id])
		book_case = Case.find(params[:case_id])
		#request_to_raspberry(:get, book_case.x_position, book_case.y_position)
		redirect_to books_path
	end

	
	
	private 

	def set_schedules(book)
		if !book.schedules.empty?
			first_schedule = book.schedules.first
			first_schedule.start_date = Time.now + 1.day
			first_schedule.end_date = Time.now + 15.days
		end
	end

	def free_case(book)
		case_book = Case.find(book.case_id)
		case_book.update_attribute(:book_id, nil)
		book.update_attribute(:case_id, nil)
	end

	def set_case(book)
		case_book = Case.find_by(book_id: nil)
		if !case_book.nil?
			case_book.update_attribute(:book_id, book.id)
			book.update_attribute(:case_id, case_book.id)
			return true
		else
			return false
		end
	end

end
