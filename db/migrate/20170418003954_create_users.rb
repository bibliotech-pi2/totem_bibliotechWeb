class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :cpf
      t.string :matricula
      t.string :email
      t.string :telefone
      t.date :dataNascimento
      t.string :endereco
      t.integer :numero
      t.string :complemento
      t.string :cep
      t.string :password_digest  
      t.timestamps
    end
  end
end
