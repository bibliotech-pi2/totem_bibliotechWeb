module LoansHelper
  def commands
    { get: 'a', put: 'b' }
  end

	def request_to_raspberry(cmd, x, y)
    message = "#{commands[cmd]}#{x.chr}#{y.chr}"
    puts "message = #{message}"
		Rails.tcp_socket.print(message)
	end
end
