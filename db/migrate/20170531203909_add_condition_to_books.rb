class AddConditionToBooks < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :condition_deactivate, :boolean, default: false
  end
end
