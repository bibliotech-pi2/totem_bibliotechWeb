# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170531203909) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "books", force: :cascade do |t|
    t.string   "author_one"
    t.string   "author_two"
    t.string   "author_three"
    t.string   "title"
    t.string   "subtitle"
    t.string   "publisher"
    t.integer  "year"
    t.string   "city"
    t.integer  "edition"
    t.integer  "volume"
    t.integer  "pages"
    t.string   "ISBN"
    t.string   "barcode"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.boolean  "available"
    t.integer  "case_id"
    t.boolean  "condition_deactivate", default: false
    t.index ["case_id"], name: "index_books_on_case_id", using: :btree
  end

  create_table "cases", force: :cascade do |t|
    t.integer  "x_position"
    t.integer  "y_position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "book_id"
    t.index ["book_id"], name: "index_cases_on_book_id", using: :btree
  end

  create_table "loans", force: :cascade do |t|
    t.datetime "loan_date"
    t.datetime "return_date"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "book_id"
    t.integer  "user_id"
    t.date     "real_return_date"
    t.index ["book_id"], name: "index_loans_on_book_id", using: :btree
    t.index ["user_id"], name: "index_loans_on_user_id", using: :btree
  end

  create_table "schedules", force: :cascade do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.integer  "book_id"
    t.index ["book_id"], name: "index_schedules_on_book_id", using: :btree
    t.index ["user_id"], name: "index_schedules_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "cpf"
    t.string   "matricula"
    t.string   "email"
    t.string   "telefone"
    t.date     "dataNascimento"
    t.string   "endereco"
    t.integer  "numero"
    t.string   "complemento"
    t.string   "cep"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_foreign_key "books", "cases"
  add_foreign_key "cases", "books"
  add_foreign_key "loans", "books"
  add_foreign_key "loans", "users"
  add_foreign_key "schedules", "books"
  add_foreign_key "schedules", "users"
end
